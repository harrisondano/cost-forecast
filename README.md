# Cost Forcast #

A demo webapp toforecast the cost of our infrastructure based on customer usage and expected growth.
Coded by Harrison Dano <harrisondano@gmail.com>

## Technologies Used ##

* PHP 7.4.3
* Apache 2.4.41
* HTML5
* CSS3
* Javascript

## Details ##

The number of servers required for our platform is based on the number of studies send to our environment such as x-ray, CT Scan and MRI. 
  
### Input parameters ###

* Current Number of study per day (we consider they are evenly distributed)
* Number of Study Growth per month in % 
* Number of months to forecast 
  
### How to calculate the cost ###

* RAM is one of the costlier components. We only need to have enough RAM for one day of study.
* 1000 studies require 500 MB RAM. The cost of 1GB of RAM per hour is 0.00553  USD 
* Studies are kept indefinitely. 1 study use 10 MB of storage. 1 GB of storage cost 0.10 USD per month. 

Other costs are considered to be negligible compared to the above and are not considered in this exercise. We do not consider the increase of cost within a month, for example, with a current number of study of 1000 and a growth of 3%, the new number of study is 1030 at the end of the month and the cost for that full month will be calculated with 1030 studies. 

### Output ###
  
The output should be a table with the following displayed: 

* Month Year, for example: “Jan 2020” 
* Number studies, for example “10,000,000” 
* Cost forecasted, for example “$1,347.66” 

If the number of month to forecast is “12”, the table should contain 12 rows for each month from the beginning to the end of the period with the above information.


